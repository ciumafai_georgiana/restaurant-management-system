package Observer;


import java.util.Observable;

public abstract class Subiect implements Observer {
    public abstract void notifyObserver(Observer o);
}
