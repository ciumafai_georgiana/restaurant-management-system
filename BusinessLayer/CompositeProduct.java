package BusinessLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class CompositeProduct extends MenuItem {
   private List<BaseProduct> baseProductList;

   private List<MenuItem> menuItems;


    public CompositeProduct(String name, List<BaseProduct> baseProductList) {
        this.nume = name;
        this.baseProductList = baseProductList;
    }

    public void addBaseProduct(BaseProduct b){
        baseProductList.add(b);

    }
    public void computePrice(){
        for(BaseProduct b:baseProductList){
            this.price+=b.getPrice();
        }
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<BaseProduct> getBaseProductList() {
        return baseProductList;
    }

    @Override
    public String toString(){
        String ingredients="";
        for(BaseProduct b:baseProductList){
            ingredients+=b.toString()+"";
        }
        return ingredients;
    }

}
