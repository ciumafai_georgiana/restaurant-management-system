package BusinessLayer;

import java.util.Date;

public class Order extends Restaurant{

    private int orderID;
    private Date date;
    private int tableNo;

    public Order(int orderID, Date date, int tableNo) {

        this.orderID = orderID;
        this.date = date;
        this.tableNo = tableNo;
    }

    public Order(int tableNo) {
        this.tableNo = tableNo;
    }

    public Order() {
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTableNo() {
        return tableNo;
    }

    public void setTableNo(int tableNo) {
        this.tableNo = tableNo;
    }
    @Override
    public int hashCode() {
    int result = tableNo;
    return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Order other = (Order) obj;
        if (orderID != other.orderID)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderID=" + orderID +
                ", date=" + date +
                ", tableNo=" + tableNo +
                '}';
    }
}
