package BusinessLayer;

public class BaseProduct extends MenuItem {
    private int price;

    public BaseProduct(int price, String name) {
        this.price = price;
        this.nume = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


    @Override
    public String toString(){
        return this.nume+" ";
    }
}
