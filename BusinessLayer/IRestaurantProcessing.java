package BusinessLayer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public interface IRestaurantProcessing {
    /**
     * @pre !menuItems.contains(menuItem)
     * @post !menuItems.isEmpty()
     */
    public void addMenuItem(MenuItem menuItem);

    public void editMenuItem(MenuItem menuItem,String p);

    public void deleteMenuItem(String nume);

    public void addOrder(Order order, ArrayList<MenuItem> menuItems);
    public void computePrice(Order order);
    public void generateBill(Order order);
}
