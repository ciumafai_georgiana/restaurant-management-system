package BusinessLayer;

import DataLayer.FileW;
import PresentationLayer.ChefGui;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;


public class Restaurant  implements IRestaurantProcessing {

    private HashMap<Order, ArrayList<MenuItem>> orders=new HashMap<Order, ArrayList<MenuItem>>();
    private PrintWriter writer;
    private List<MenuItem> menuItems=new ArrayList<MenuItem>();
    //private ArrayList<Observer> obs=new ArrayList<Observer>();
    //private List<Order> comenzi=new ArrayList<Order>();

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public List<Order> getOrders(){
        Set<Order> keySet=orders.keySet();
        //System.out.println("getOrders");
        ArrayList<Order> list=new ArrayList<Order>(keySet);
        //System.out.println(list);
        return list;
    }

    public void addMenuItem(MenuItem menuItem) {
        if(!menuItems.contains(menuItem)){
            menuItems.add(menuItem);

        }
    }



    public void editMenuItem(MenuItem menuItem,String price) {

            for(MenuItem item : menuItems){
                    if (item.getNume().equals(menuItem.getNume())) {
                        //item.setName(menuItem.getNume());
                        item.setPrice(Integer.parseInt(price));
                        break;
                    }


            }

    }

    public void deleteMenuItem(String nume) {
            for(MenuItem item : menuItems) {
                if (item.getNume().equals(nume)) {
                    menuItems.remove(item);
                    break;
                }
            }

    }

    private String msg=new String();
    public void addOrder(Order order,ArrayList<MenuItem>menuItems) {
        for(Order o:orders.keySet()){
            if(o.hashCode()==order.hashCode()){
                    orders.put(order,menuItems);
                    msg+=o.toString();
            }
        }
    }
    public void createOrder(Order order,ArrayList<MenuItem>menuItems) {
        orders.put(order,menuItems);
       // notify();
    }

    public void computePrice(Order order) {
        int price=0;
        for(Order o:orders.keySet()){
            if(o.getOrderID()==order.getOrderID()){
                ArrayList<MenuItem>m2=orders.get(o);
                    for(MenuItem m:m2){
//                        if(m instanceof CompositeProduct){
//                            m.computePrice();
//                        }
                        price+=m.getPrice();

                    }
                }

        }
        System.out.println("restaurant.computePrice():"+price);
    }

    public void generateBill(Order order) {
        FileW fileWriter=new FileW();
        fileWriter.generateBillFile(order,orders);
    }

    @Override
    public String toString() {
        String rez="";
        for(Order o:orders.keySet()){

            rez+=o.toString();

        }
        if(orders.keySet().isEmpty()){
            System.out.println("restaurant.toString():empty");
        }
        return rez;
    }





}
