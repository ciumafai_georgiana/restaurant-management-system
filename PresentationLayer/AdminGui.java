package PresentationLayer;

import BusinessLayer.IRestaurantProcessing;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminGui implements IRestaurantProcessing{

        JTable tableA=new JTable();

        JFrame frame = new JFrame("Administrator");
        JPanel panel2=new JPanel();
        private JPanel panel = new JPanel();
        private JButton AddMenuItem = new JButton("Add new MenuItem");
        private JButton DeleteMenuItem = new JButton("Delete MenuItem");
        private JButton EditMenuItem = new JButton("Edit MenuItem");
        private JButton ViewAllMenuItem = new JButton("View all MenuItems");
        private JLabel name=new JLabel("Nume:");
        private JLabel price=new JLabel("Price:");
        private JTextField numeT=new JTextField(10);
        private JTextField priceT=new JTextField(10);
        //        private JScrollPane scrollPaneP = new JScrollPane();
        private JScrollPane scrollPaneA = new JScrollPane();

        public AdminGui() {
            frame.add(panel);
            panel.add(panel2);
            panel.add(AddMenuItem);
            panel.add(DeleteMenuItem);
            panel.add(EditMenuItem);
            panel.add(ViewAllMenuItem);


            scrollPaneA.setViewportView(tableA);
            panel.add(scrollPaneA);
            panel2.add(name);
            panel2.add(numeT);
            panel2.add(price);
            panel2.add(priceT);
           frame.pack();
            frame.setVisible(true);
        }


        // punem datele in tabel
        void setTableA(JTable newtable)
        {

            this.tableA=newtable;
            scrollPaneA.setViewportView(tableA);
            //scrollPaneA.setBounds(20,220,500,500);
            panel.add(scrollPaneA);

        }

        JTable getTableA()
        {
            return tableA;
        }


    public String getNumeT() {
        return numeT.getText();
    }

    public void setNumeT(JTextField numeT) {
        this.numeT = numeT;
    }

    public String getPriceT() {
        return priceT.getText();
    }

    public void setPriceT(JTextField priceT) {
        this.priceT = priceT;
    }
    void AddMenuItemListener(ActionListener p1) {
        AddMenuItem.addActionListener(p1);
        //System.out.println("aaaaa");
    }

    void EditMenuItemListener(ActionListener p2) {
        EditMenuItem.addActionListener(p2);
    }

    void DeleteMenuItemListener(ActionListener p3) {
        DeleteMenuItem.addActionListener(p3);
    }

    void ViewMenuItemListener(ActionListener p4) {
        ViewAllMenuItem.addActionListener(p4);
    }

    void showTable(ArrayList<MenuItem> menuItems){

    }
    public void addMenuItem(MenuItem menuItem) {
    }

    public void editMenuItem(MenuItem menuItem,String p) {

    }

    public void deleteMenuItem(String nume) {

    }

    public void addOrder(Order order, ArrayList<MenuItem> menuItems) {

    }


    public void computePrice(Order order) {

    }

    public void generateBill(Order order) {

    }

}


