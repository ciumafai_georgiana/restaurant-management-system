package PresentationLayer;

import BusinessLayer.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.Date;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Controller {
    AdminGui adminGui;
    ChefGui chefGui;
    WaiterGui waiterGui;
    Restaurant restaurant;
    public  static int orderid=1;
    Date date=new Date();
    public Controller(AdminGui adminGui,ChefGui chefGui,WaiterGui waiterGui,Restaurant restaurant){
        adminGui=adminGui;
        chefGui=chefGui;
        waiterGui=waiterGui;
        restaurant=restaurant;


    }

    public Controller(AdminGui adminGui, Restaurant restaurant,WaiterGui waiterGui)
    {
        this.adminGui=adminGui;
        this.restaurant=restaurant;
        this.waiterGui=waiterGui;
        adminGui.AddMenuItemListener(new AddMenu());
        adminGui.DeleteMenuItemListener(new DeleteMenu());
        waiterGui.AddOrder(new AddOrders());
        waiterGui.ComputeBills(new CB());
        adminGui.EditMenuItemListener(new ED());

    }
    private class ED implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            List<MenuItem> menuItems = restaurant.getMenuItems();
            String names = adminGui.getNumeT();
            //String price = adminGui.getPriceT();
            //System.out.println(names);
            for (MenuItem m : menuItems) {
                if (m.getNume().equals(names)) {
                    restaurant.editMenuItem(m,adminGui.getPriceT());
                }
            }
            showMenuTable(restaurant.getMenuItems());
        }
    }
    private class AddMenu implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
               // System.out.println("inainte");
                String names=adminGui.getNumeT();
                String price=adminGui.getPriceT();
                System.out.println(names);
                String[] product = names.split(":");

                if(product.length==1){
                    int pr= Integer.parseInt(price);
                    BaseProduct baseProduct=new BaseProduct(pr,product[0]);
                    restaurant.addMenuItem(baseProduct);
                }
                else{
                    String[] bpNames= product[1].split(",");
                    ArrayList<BaseProduct> bplist=new ArrayList<BaseProduct>();
                    for( String bpName: bpNames) {
                        for (MenuItem i : restaurant.getMenuItems()) {
                            if(i.getNume().equals(bpName)){
                                bplist.add((BaseProduct) i);
                                break;
                            }
                        }
                    }
                    CompositeProduct cp= new CompositeProduct(product[0],bplist);
                    cp.computePrice();
                    restaurant.addMenuItem(cp);
                }

                showMenuTable(restaurant.getMenuItems());

//              BaseProduct p=new BaseProduct(12,"cola");
//              BaseProduct p2= new BaseProduct(14,"Georgi la pachet");
//              ArrayList<BaseProduct> prds= new ArrayList<BaseProduct>();
//              prds.add(p);
//              prds.add(p2);
//              CompositeProduct cmp= new CompositeProduct("Georgi  cu cola",prds);
//              restaurant.addMenuItem(p);
//              restaurant.addMenuItem(p2);
//              restaurant.addMenuItem(cmp);
//              System.out.println("apaas");
//              showMenuTable(restaurant.getMenuItems());


            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class DeleteMenu implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                List<MenuItem>menuItems=restaurant.getMenuItems();
                String names=adminGui.getNumeT();
                String price=adminGui.getPriceT();
                //System.out.println(names);
                restaurant.deleteMenuItem(names);

                showMenuTable(restaurant.getMenuItems());
            }catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private void showMenuTable(List<MenuItem> items){
        String[] columnName = new String[3];
        columnName[0]="Nume";
        columnName[1]="Descriere";
        columnName[2]="Pret";

        DefaultTableModel model = new DefaultTableModel(columnName,0);
        Object[] row= new Object[3];
        for(MenuItem i : items) {
            //System.out.println("aaa");
            if (i instanceof BaseProduct) {
                row[0] = i.getNume();
                row[1] = "";
                row[2] = i.getPrice();
            }
            if (i instanceof CompositeProduct) {
                row[0] = i.getNume();
                String descriere="";
                List<BaseProduct> baseProducts=((CompositeProduct) i).getBaseProductList();
                for(int b=0; b<baseProducts.size();b++){
                    if(b==baseProducts.size()-1){
                        descriere+=baseProducts.get(b).getNume();
                    }else{
                        descriere+=baseProducts.get(b).getNume()+", ";
                    }

                }
                row[1]=descriere;
                row[2]=i.getPrice();
            }
            model.addRow(row);

        }
        JTable t=new JTable(model);
        adminGui.setTableA(t);

    }

    private void showMenuTable2(List<MenuItem> items){
        String[] columnName = new String[3];
        columnName[0]="Nume";
        columnName[1]="Descriere";
        columnName[2]="Pret";

        DefaultTableModel model = new DefaultTableModel(columnName,0);
        Object[] row= new Object[3];
        for(MenuItem i : items) {
            //System.out.println("aaa");
            if (i instanceof BaseProduct) {
                row[0] = i.getNume();
                row[1] = "";
                row[2] = i.getPrice();
            }
            if (i instanceof CompositeProduct) {
                row[0] = i.getNume();
                String descriere="";
                List<BaseProduct> baseProducts=((CompositeProduct) i).getBaseProductList();
                for(int b=0; b<baseProducts.size();b++){
                    if(b==baseProducts.size()-1){
                        descriere+=baseProducts.get(b).getNume();
                    }else{
                        descriere+=baseProducts.get(b).getNume()+", ";
                    }

                }
                row[1]=descriere;
                row[2]=i.getPrice();
            }
            model.addRow(row);

        }

        JTable t2=new JTable(model);
        waiterGui.setTable(t2);


    }



    //    protected void handleSelectionEvent(ListSelectionEvent e) {
//        if (e.getValueIsAdjusting())
//            return;
//
//        // e.getSource() returns an object like this
//        // javax.swing.DefaultListSelectionModel 1052752867 ={11}
//        // where 11 is the index of selected element when mouse button is released
//
//        String strSource= e.getSource().toString();
//        int start = strSource.indexOf("{")+1,
//                stop  = strSource.length()-1;
//        iSelectedIndex = Integer.parseInt(strSource.substring(start, stop));
//    }
    private void showOrderTable(List<Order> items){
        String[] columnName = new String[3];
        columnName[0]="Id";
        columnName[1]="Data";
        columnName[2]="Numar masa";

        DefaultTableModel model = new DefaultTableModel(columnName,0);
        Object[] row= new Object[3];
        for(Order i : items) {
            //System.out.println("aaa");
            row[0] = i.getOrderID();
            row[1] = date;
            row[2] = i.getTableNo();
            model.addRow(row);

        }
        JTable t=new JTable(model);
        waiterGui.setTableW(t);
    }
    private class AddOrders implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem>menuItems2=new ArrayList<MenuItem>();
            try {
                    // System.out.println("inainte");
                    String names=waiterGui.getPrd();
                    String[] products = names.split(",");
                    int cont=0;
                    int len=products.length;
                    if(products.length==1){
                        for(MenuItem m:restaurant.getMenuItems()) {
                            if (products[0].equals(m.getNume())) {
                                //BaseProduct baseProduct = new BaseProduct(m.getPrice(), products[0]);
                                Order o = new Order(orderid, date, Integer.parseInt(waiterGui.getNrm()));
                                menuItems2.add(m);
                                restaurant.createOrder(o, menuItems2);
                            }
                        }
                    }
                    else {

                            for (MenuItem i : restaurant.getMenuItems()) {
                                for (int j=0;j<products.length;j++) {
                                    if (products[j].equals(i.getNume())) {
                                        menuItems2.add(i);


                                    }
                                }

                            }

                        Order o2 = new Order(orderid, date, Integer.parseInt(waiterGui.getNrm()));
                        restaurant.createOrder(o2, menuItems2);
                        System.out.println(menuItems2.size());
                    }
                showMenuTable2(restaurant.getMenuItems());
                showOrderTable(restaurant.getOrders());
                orderid++;

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class CB implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int nrm=Integer.parseInt(waiterGui.getNrm());

            for(Order o:restaurant.getOrders()){
                if(o.getTableNo()==nrm){
                    restaurant.generateBill(o);
                    System.out.println("Table number"+o.getTableNo());
                    restaurant.computePrice(o);

                    //System.out.println("Table number"+);
                }
            }
        }
    }
}
