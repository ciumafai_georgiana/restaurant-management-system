package PresentationLayer;


import Observer.Observer;
public class ChefGui implements Observer {


    @Override
    public void update(String msg) {
        System.out.println(msg);
    }
}
