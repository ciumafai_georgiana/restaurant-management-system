package PresentationLayer;

import BusinessLayer.IRestaurantProcessing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;

public class WaiterGui   {
        JTable tableW=new JTable();
        JTable table=new JTable();

        JFrame frame = new JFrame("Waiter");

        private JPanel panel = new JPanel();
        private JPanel panel2=new JPanel();
        private JButton NewOrder = new JButton("Add new Order");
        private JButton ComputeBill = new JButton("Compute Bill");
        private JButton ViewAll = new JButton("View All Orders");
        private JScrollPane scrollPaneA=new JScrollPane();
        private JScrollPane scrollPane=new JScrollPane();
        private JLabel nrmasa=new JLabel("Nr masa");
        private JLabel produs=new JLabel("Produs");
        private JTextField nrm=new JTextField(10);
        private JTextField prd=new JTextField(10);
        public WaiterGui() {
            frame.add(panel);
            panel.add(panel2);
            panel.add(NewOrder);
            panel.add(ComputeBill);
            panel.add(ViewAll);

            scrollPaneA.setViewportView(tableW);
            scrollPane.setViewportView(table);
            panel.add(scrollPaneA);
            panel.add(scrollPane);
            panel2.setAlignmentX(Component.LEFT_ALIGNMENT);
            panel2.add(nrmasa);
            panel2.add(nrm);
            panel2.add(produs);
            panel2.add(prd);

            frame.pack();
            frame.setVisible(true);
        }


        // punem datele in tabel
        void setTableW(JTable newtable)
        {
            this.tableW=newtable;
            scrollPaneA.setViewportView(tableW);
            //scrollPaneA.setBounds(20,220,500,500);
            panel.add(scrollPaneA);
        }

        JTable getTableW()
        {
            return tableW;
        }
    // punem datele in tabel
         void setTable(JTable newtable)
         {
        this.table=newtable;
        scrollPane.setViewportView(table);
        //scrollPaneA.setBounds(20,220,500,500);
        panel.add(scrollPane);
         }

          JTable getTable()
        {
        return table;
        }

        void AddOrder(ActionListener p1) {
            NewOrder.addActionListener(p1);
            //System.out.println("aaaaa");
         }
         void ComputeBills(ActionListener p2) {
        ComputeBill.addActionListener(p2);
    }

        void ViewAll(ActionListener p3) {
        ViewAll.addActionListener(p3);
    }

    public String getNrm() {
        return nrm.getText();
    }

    public void setNrm(JTextField nrm) {
        this.nrm = nrm;
    }

    public String getPrd() {
        return prd.getText();
    }

    public void setPrd(JTextField prd) {
        this.prd = prd;
    }
}
