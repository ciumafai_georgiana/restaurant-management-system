package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class FileW {

    public void generateBillFile(Order order,HashMap<Order,ArrayList<MenuItem>>orders) {
        PrintWriter writer;
        try {
            writer =new PrintWriter("Bill.txt", "UTF-8");
            int totalPrice=0;
            for(Order o:orders.keySet()){
                if(o.hashCode()==order.hashCode()) {
                    if (o.getOrderID() == order.getOrderID()) {
                        writer.println("   Order number:" + o.getOrderID());
                        writer.println("   Produse:");
                        ArrayList<MenuItem>m2=orders.get(o);
                        for(MenuItem m:m2){
                            //m.computePrice();
                            writer.println("      Nume: " +m.getNume()+ " " + "Pret: "  + m.getPrice());
                            totalPrice+=m.getPrice();
                        }

                    }
                }
            }
            writer.println("   Pret Total: " + totalPrice);
            writer.println("   ----------------------------");
            writer.println("   Restaurant: Casa Via");
            writer.close();
        }catch(Exception e) {
            System.out.println(e.getMessage());
        }



    }
}
